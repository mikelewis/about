# About Me

I am the manager of the Technical Writing team at GitLab.

As a technical writer, I have worked on software supporting DevOps, financial services,
media, and marketing. In addition to my 13 years as a full-time technical writer or tech writing manager,
I was also part of a technical support team for two years, and founded a music news website, focused on
live music reviews and photography.

I am a co-organizer of [Write The Docs NYC](https://www.meetup.com/WriteTheDocsNYC/).

## Documentation philosphy

I believe in the principle of meeting readers where they are, offering content that is as easy as possible to find, browse, and understand. It should be authored with the audience clearly in mind, empowering readers whether they wish to learn about products, features, and use cases, or solve specific problems.

I am glad to be part of a team which seeks to assist users through clear UX copy within the product itself, preventing the need to access documentation when it is not necessary.

## How I work

I enjoy applying the [GitLab Values](https://about.gitlab.com/handbook/values/) to how I work as an individual and how my team works.

As a manager, I work to balance decisiveness with an open-mindedness to new views and ideas.

I am always seeking to improve my knowledge of areas of GitLab, DevOps, and doc tools where I may have less experience, and to iterate on my own process and team process.

I believe in the value of both live and asynchronous communication, and in conducting both efficiently in order to maintain work velocity and sufficent time for deep work. This includes establishing clear agendas and follow-up tasks for any meetings, and, of course, documentation of discussions. I believe in the value of diagrams, and often craft a diagram whenever a topic reaches a higher level of complexity.

## What my direct reports should know

* Let me know if I am blocking you or if I should be aware of other blockers you may have. One of my main responsibilities is ensuring that we are set up for success. Please reach out and let me know how I can help.
* I prefer over-communication to under-communication. Ping me on Slack or add an FYI to our agenda doc any time.
* I appreciate any and all feedback on what's working or could be better in your day to day work, in my support of your work, in our work as a team, or anything else. All feedback and ideas are welcome.
* If you have questions that are not answered here, let me know!
